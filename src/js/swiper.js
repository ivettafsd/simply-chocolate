import Swiper from 'swiper/bundle';

import 'swiper/css/bundle';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export const buildSwiperSlider = sliderElm => {
  const sliderIdentifier = sliderElm;
  const swiper = new Swiper(`[data-id="${sliderIdentifier}"]`, {
    slidesPerView: 1,
    spaceBetween: 18,
    breakpoints: {
      768: {
        slidesPerView:
          sliderIdentifier === 'review'
            ? 2
            : sliderIdentifier === 'buy'
            ? 3
            : 2.5,
      },
      1200: {
        slidesPerView:
          sliderIdentifier === 'review' || sliderIdentifier === 'buy' ? 3 : 4,
      },
    },
    pagination: {
      el: `.swiper-pagination-${sliderIdentifier}`,
      type: 'bullets',
      clickable: true,
    },
  });
  return swiper;
};

buildSwiperSlider('buy');
buildSwiperSlider('products');
buildSwiperSlider('review');
