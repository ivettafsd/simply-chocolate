# Simply Chocolate

Landing page for the 'Simply Chocolate'
[Посилання на живу сторінку](https://ivetta-chocolate.netlify.app/)

Всі матеріали до проєкту:
[Макет](https://www.figma.com/file/LWMTodUscRGMcbTpxE3kgI/Simply-Chocolate?node-id=0%3A1&t=KO70kXV8eNG9H1AO-1)
[Технічне завдання](https://docs.google.com/document/d/1s-V_iLMJUicgxsCyjSIfYeRZ-MvYuW0frLXkUxNktYc/edit?usp=sharing)